const mongoose = require("mongoose");
mongoose.set('strictQuery', true);

const userSchema = new mongoose.Schema({
	fullName : {
		type : String,
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},

	orders : [{products : [{
					productId:{
						type: String,
						required: [true, "ProductId is required"]
					},
					quantity:{
							type: Number,
							required: [true, "Quantity is required"]
					}
				}],

			totalPrice: {
				type: Number
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);