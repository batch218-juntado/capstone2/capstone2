// dependencies
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// routers
const userRoutes = require("./routes/userRoute.js")
const productRoutes = require("./routes/productRoute.js");
const orderRoutes = require("./routes/orderRoute.js");

// create an express server
const app = express();

// middlewares
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// initializing the routes
app.use("/users", userRoutes);
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);


// Connect to MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@capstone2.gckki7r.mongodb.net/capstone2?retryWrites=true&w=majority",
	{
	useNewUrlParser: true,
	useUnifiedTopology: true
});


mongoose.connection.once('open', () => console.log('Now connected to Capstone 2 DB Atlas.'));


app.listen(process.env.PORT || 4000, () => 
	{console.log(`API is now online on port ${process.env.PORT || 4000}`)
});

/*
Capstone 2
- User Registration/
- User Authentication/
- Create Product (Admin Only)/
- Retrieve All Active Products/
- Retrieve Single Product/
- Update Product Information (Admin Only)/
- Archive Product (Admin Only)/
- Non-Admin User Checkout (Create Order)
- Retrieve User Details/

Stretch Goals
- Set User as Admin (Admin Only)
- Retrieve authenticated user's orders
- Retrieve all orders (Admin Only)
- Add to Cart
	- Added Products
	- Change Product Quantities
	- Remove Products from Cart
	- Subtotal for each Item
	- Total Price for All Items
*/