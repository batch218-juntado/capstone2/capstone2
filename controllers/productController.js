const User = require("../models/user.js");
const Product = require("../models/product.js");


// Add a product (Admin)
module.exports.addProduct = (data) => {
		if(data.isAdmin){
			let newProduct = new Product({
				productName: data.product.productName,
				description: data.product.description,
				price: data.product.price
			});

			return newProduct.save().then((newProduct, error) => {
				if(error){
					return error
				}

				return newProduct 
			})
		}
		else{
			let message = Promise.resolve('User must be ADMIN to update this product.');
			return message.then((value) => {

			return value
		});
	}
}


// Get All Products
module.exports.getAllProducts = () => {
    return Product.find({}).then(result => {
        return result;
    })
}



// Get All Active Products
module.exports.getActiveProducts = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}



// Retrieve a Single Product
module.exports.getProduct = (productId) => {

	return Product.findById(productId).then(result => {		
		return result;
	})
}



// Update Product (Admin)
module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin){
		return Product.findByIdAndUpdate(productId, 
			{			
				productName: newData.product.productName,
				description: newData.product.description,
				price: newData.product.price

			}).then((updatedProduct, error) => {
			
			if(error){
				return false
			}
			return updatedProduct;
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to update this product.');
		return message.then((value) => {

			return value
		});
	}
}


// Archive Product
module.exports.archiveProduct = (productId, newData) => {
	if(newData.isAdmin){
		return Product.findByIdAndUpdate(productId, 
				{
					isActive: false
				}
			).then((updatedProduct, error) =>{
				if(error){
					return false
				}
				else{
					return "Successfully archived product."
				}
			})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this.');
		return message.then((value) => {return value})
	}
}

