const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/product.js");
const Order = require("../models/order.js");

// Register a User
module.exports.registerUser = (reqBody) => {
		let newUser = new User({
		fullName: reqBody.fullName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		if(error){
			return false;
		}
		else{
			return true;
		}
	})
	
};

// Login User
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result =>{
		if(result == null){
			return false;
		}
		else{
			console.log(result.password)
			// compareSync is bcrypt function to compare a unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)};
			}
			else{
				// if password do not match
				return false;
				// return "Incorrect password"
			}
		}
	})
}

// Create Order
module.exports.addOrder = async (data) => {
	if(data.isAdmin == false){
		let isUserUpdated = await User.findById(data.userId).then(user => {

			user.orders.push({
				products: {
					productId: data.productId, 
					quantity: data.quantity
				}
			});


			return user.save().then((user,error) => {
				if(error){
					return false;
				}
				else{
					return true;
				};
			});
		});

		let isProductUpdated  = await Product.findById(data.productId).then(product => {

			product.orders.push({userId: data.userId});

			return product.save().then((product, error) =>{
				if(error){
					return false;
				}
				else{
					return true;
				}
			});
		});

		if (isUserUpdated && isProductUpdated){
			return true;
		}
		else{
			return false;
		};
	}

	else{
		return "Please log-out of your admin account to order our products."
	}

};



// Retrieve/Get User Details
module.exports.getInfo = (userId) => {
	return User.findById(userId).then((info, error) =>{
		if(error){
			
			return error
		}
		
		else{
			
			info.password = ""
			
			return info
		}
	})
};


// Set as Admin (Admin Only)
module.exports.roleAdmin = (userId, update) => {
	if(update.isAdmin == true){
		return User.findByIdAndUpdate(userId, 

				{
					isAdmin: true
				}

			).then((updatedRole, error) =>{
				if(error){
					return false
				}
				else{
					return "Successfully set user as ADMIN."
				}
			})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this.');
		return message.then((value) => {
			return value
		})
	}
};

