const User = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/product.js");
const Order = require("../models/order.js");
