const express = require("express"); 
const router = express.Router(); 
const User = require("../models/user.js");
const auth = require("../auth.js");
const userController = require("../controllers/userController.js");

// Register User
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

// Login User
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(
		resultFromController => res.send(resultFromController
			))
})

// Get User Info
router.get("/info/:userid", (req, res) => {
	userController.getInfo(req.params.id).then(result => res.send(result))
});

// Create Order
router.post("/addorder", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.products.productId,
		quantity: req.body.products.quantity
	}
	userController.addOrder(data).then(result => res.send(result))
});

// Set Admin (Admin Only)
router.patch("/auth/:userid", auth.verify, (req, res) => {
	const role = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.roleAdmin(req.params.id, role).then(result => res.send(result))
});


module.exports = router;