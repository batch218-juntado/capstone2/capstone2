const express = require("express");
const router = express.Router();
const Product = require("../models/product.js");
const auth = require("../auth.js");
const productController = require("../controllers/productController.js");

// Add a Product (Admin)
router.post("/addproduct", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

productController.addProduct(data).then(resultFromController => res.send(resultFromController));
});


// Get All Products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
})


// Get All Active Products
router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
})


// Get a Product 
router.get("/:productId", (req, res) => {
							// retrieves the id from the url
	productController.getProduct(req.params.productId).then(resultFromController => res.send(resultFromController))
})


// Update a Product (Admin)
router.patch("/:productId/update", auth.verify, (req,res) => {
	const newData = {
		product: req.body, 	
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.updateProduct(req.params.productId, newData).then(resultFromController => {
		res.send(resultFromController)
	});
});


// Archive a Product (Admin)
router.patch("/archive/:productId", auth.verify, (req, res) => {
	const update = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	productController.archiveProduct(req.params.productId, newData).then(result => res.send(result))
});



module.exports = router;